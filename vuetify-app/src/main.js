import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import vuetify from './plugins/vuetify';
import Home from './views/Home.vue'
import About from './views/About.vue'
import ContactUs from './views/ContactUs.vue'
import Login from './views/Login.vue'
import Profile from './views/Profile.vue'


Vue.config.productionTip = false
Vue.use(VueRouter);

const routes = [
  {path:'/', component: Home},
  { path: '/about', component: About},
  { path: '/Contact_us', component: ContactUs},
  { path: '/profile', component: Profile},
  { path: '/login', component: Login},
];

const router = new VueRouter({
  routes,
  mode:'history'
})

new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
